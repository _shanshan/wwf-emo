import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import MtaH5 from 'mta-h5-analysis';

Vue.config.productionTip = false;
// 按需引入 
import 'vue-ydui/dist/ydui.base.css';
import { Toast ,Loading} from 'vue-ydui/dist/lib.rem/dialog';


// import Vconsole from 'vconsole'
// let vConsole = new Vconsole()
// Vue.use(vConsole)

Vue.prototype.$dialog = {
  toast: Toast,
  loading:Loading
};
MtaH5.init({
  "name":'MTAH5',
  "sid":'500723153', //必填，统计用的appid
  "cid":'500723154', //如果开启自定义事件，此项目为必填，否则不填
  "autoReport":0,//是否开启自动上报(1:init完成则上报一次,0:使用pgv方法才上报)
  "senseHash":0, //hash锚点是否进入url统计
  "senseQuery":1, //url参数是否进入url统计
  "performanceMonitor":0,//是否开启性能监控
  "ignoreParams":[] //开启url参数上报时，可忽略部分参数拼接上报
});

new Vue({
  router,
  render: h => h(App),
  data(){
    return{
      paintURL:'',
      postURL:'',
      click_id:''
    }
  }
}).$mount("#app");
