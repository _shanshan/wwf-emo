module.exports = {
    //打包的基本路径
    publicPath: process.env.NODE_ENV === "production" ? "./" : "./",
    //打包后文件名
    outputDir: 'dist'
};